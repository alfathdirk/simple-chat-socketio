const http =  require('http');
const https =  require('https');
const fs = require('fs');
const { mode, ssl } = require('./config');

module.exports = function({ app }) {
    let server = http.Server(app.callback());
    if(mode != 'DEV') {
        server = https.createServer({
            key: fs.readFileSync(ssl.key,'utf-8'),
            cert: fs.readFileSync(ssl.cert,'utf-8')
        },app.callback());
    }
    return server;
}
