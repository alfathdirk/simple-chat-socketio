let Koa = require('koa');
let serve = require('koa-static');
let jwt = require('jsonwebtoken');
let fetch = require('node-fetch');

let manager = require('./manager');
let config = require('./config');

let app = new Koa();

// app.use(router.routes())
app.use(serve(require('path').resolve(__dirname) + '/../www/'));

let server = require('./server')({ app });

let io = require('socket.io')(server)

async function checkExistRoom(roomid) {
  let exist = false;
  await manager.runSession(async(session) => {
    let data = await session.factory('ChatRoom',{ RoomId: roomid }).single();
    if(data) {
      exist = true;
    }
  })
  return exist;
}

async function verifyData(to, auth, appname) {
  let data = {};
  await manager.runSession(async (session) => {
    let { UserId, name: myName } = jwt.verify(auth, config.jwt.key);
    try {

      let { UserId: UserIdTo, UserName, name, role } = await session.factory('Users', { UserName: to, appname: appname.toLowerCase() }).single();
      let roomId = [UserIdTo, UserId].sort().toString();
      data = { UserId, UserName, roomId, name, role, myName, UserIdTo }
    } catch(err) {
      console.error('Not Found Username')
    }
  });
  return data;
}

io.on('connection', (socket) => {
  socket.on('userHandler', async ({ to, auth, socketId, appname }) => {
    try {
      let { UserId, UserName, roomId, name, role } = await verifyData(to, auth, appname);
      manager.runSession(async(session) => {
        let existRoom = await checkExistRoom(roomId);
        if(!existRoom) {
            await session.factory('ChatRoom').insert({ RoomId: roomId }).save();
        }
        socket.join(roomId);
        let historyChat = await session.factory('ChatData', { ChatRoomId: roomId }).all()
        let data = {
          roomChatData: historyChat,
          UserId,
          UserTo: {
            UserName,
            name,
            role,
          },
        }
        io.to(socketId).emit('historyChat',data);
      })
    } catch (error) {
      console.log('error', error);
    }
  });


  socket.on('chat',async (auth, data)=> {
    try {
      let { roomId, myName, UserIdTo } = await verifyData(data.to, auth, data.appname);
      if(!roomId) {
        return;
      }

      let chatContent = {
        message: data.message,
        from: myName,
        UserId: UserIdTo,
        date: Math.floor(new Date())
      };
      await manager.runSession(async(session) => {
        io.to(roomId).emit('clientChat', chatContent);
        await session.factory('ChatData').insert({
          ChatRoomId: roomId,
          message: data.message,
          fromName: myName,
          UserId: UserIdTo,
          dateSubmit: chatContent.date
        }).save();
      });

      const head = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer '+ auth,
        },
        method: 'POST',
        body: JSON.stringify({
          title: chatContent.from,
          body: chatContent.message,
          to: UserIdTo,
          appNameTo: data.appname
        }),
      };

      try {
        let f =  await fetch(config.apiUrl+'/sendchat',head);
        return f.json();
      } catch (error) {
        console.error(error);
        return error
      }
    } catch(err) {
      console.error(err);
    }
  });
});

server.listen(config.port, "0.0.0.0",() => console.log(`running on ${config.port}, mode => ${process.env.MODE || 'production'}`));
