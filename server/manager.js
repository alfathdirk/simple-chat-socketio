const { Manager } = require('node-norm');
const { normMysql } = require('./config');

const manager = new Manager({
  connections: normMysql.connections,
});

module.exports = manager;
