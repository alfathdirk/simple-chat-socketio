let images = {
  farmer: 'https://cdn.iconscout.com/public/images/icon/premium/png-512/farmer-people-work-profession-job-avatar-3e39f0c09d9ee6a1-512x512.png',
}

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

let socket = io();
let auth = getParameterByName('token');
let to = getParameterByName('to');
let profileName = getParameterByName('name') || 'Unknown';
let appname = getParameterByName('appname') || 'cocoatrace';
let history = getParameterByName('history') || 'true';
let isWeb = getParameterByName('web') || false;
let myId = '';

function getTemplate(data) {
  $('#messagesPane').append(`
    <div class="${(data.UserId == myId) ? '' : 'my '}line">
      <span class="name">${data.from || data.fromName}</span>
      <p class="chat">${data.message}<span>${moment(data.date || data.dateSubmit).calendar()}</span></p>
    </div>
  `);
  $("#messagesPane").animate({ scrollTop: $('#messagesPane')[0].scrollHeight }, "fast");
}

socket.on('connect', () => {
  socket.emit('userHandler', { to, auth, socketId: socket.id, appname })

  socket.on('historyChat', function({ UserId, UserTo, roomChatData }) {
    myId = UserId;
    document.getElementsByClassName('profileName')[0].innerText = UserTo.name;
    document.getElementsByClassName('profileImage')[0].setAttribute('src', images[UserTo.role.toLowerCase()] || 'https://cdn.iconscout.com/public/images/icon/free/png-512/avatar-user-business-man-399587fe24739d5a-512x512.png' );
    document.getElementsByClassName('roles')[0].innerText = UserTo.role;
    if(isWeb) {
      console.log('Youre open from web');
      prepareFCM(auth);
    }
    roomChatData.map(v => {
      if(history.toLowerCase() !== 'true') {
        return;
      }
      getTemplate(v);
    });
  });

  socket.on('clientChat', function (data) {
    getTemplate(data);
  });

  $(window).on('keydown', function(e) {
    if (e.which == 13) {
      send();
      return;
    }
  });

});
function send() {
  if(document.getElementById('message-input-text').value.trim() != '') {
    let message = document.getElementById('message-input-text').value;
    socket.emit('chat', auth, { message, to, appname });
    document.getElementById('message-input-text').value = '';
    return;
  }
}

socket.on('disconnect', function() {
  socket.emit('disconnect', { auth });
})
