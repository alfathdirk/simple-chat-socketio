
var config = {
  apiKey: "AIzaSyDFtJuA7HjGiToHzXUr89HEt1-RzHZ4AXs",
  authDomain: "farmcloud-7832a.firebaseapp.com",
  databaseURL: "https://farmcloud-7832a.firebaseio.com",
  projectId: "farmcloud-7832a",
  storageBucket: "farmcloud-7832a.appspot.com",
  messagingSenderId: "13077804805"
};
firebase.initializeApp(config);

const messaging = firebase.messaging();
let currentToken;

try {
  messaging.requestPermission()
  .then(function() {
    messaging.getToken()
    .then(function(tokenFCM) {
      if (tokenFCM) {
        currentToken = tokenFCM
      } else {
        console.log('No Instance ID token available. Request permission to generate one.');
      }
    })
    .catch(function(err) {
      console.log('An error occurred while retrieving token. ', err);
    });

  })
  .catch(function(err) {
    console.log('Unable to get permission to notify.', err);
  });
} catch (error) {
  console.log('kena error',error)
}

function prepareFCM(auth) {
  if(currentToken) {
    $.ajax({
      method: "post",
      url: `http://localhost:8880/api/saveToken`,

      headers: {
        Authorization: 'Bearer '+auth,
      },
      data: {
        token: currentToken,
        appname: 'farmcloud'
      },
    })
    .done(function(data) {
      console.log(data)
    })
    .catch(err => {
      console.log('err',err.responseJSON.errors);
    })
  }
}
