'use strict';

self.addEventListener('push', function(event) {
    console.log('[Service Worker] Push Received.');
    let { data: { custom_notification } } = JSON.parse(event.data.text());
    let { body, title } = JSON.parse(custom_notification);
    const options = {
      body,
      icon: 'images/icon.png',
      badge: 'images/badge.png'
    };

    event.waitUntil(self.registration.showNotification(title, options));
  });
